# SSH Tunnel Manager

## Purpose

SSH Tunnel Manager seeks to provide a simple and documented means of maintaining backgrounded
ssh tunnels. For maximum compatibility and accessibility all implementation is in Bash, and
to give the end user the full freedom to modify logic to suit their needs this software is
released at the user's option under the terms of either:

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

## Installation

* Clone this repository
* Copy, or link, the `ssh-tunnel-manager` Bash script into a directory in your `$PATH`

## Dependencies

* `bash`
* `nc`
* `ip`
* `grep`
* `logger`

### `ssh-tunnel-manager` options:

* `-h`: Prints usage message
* `-F <path>`: Sets the tunnel config path (ssh_config format). REQUIRED!
* `-t <target host>`: Sets the target host (must be present in above config file). REQUIRED!
* `-e <path>`: Sets the ssh-tunnel-manager config path for this instance. OPTIONAL

### Tunnel Configuration

The path given after `-F` should correspond to a valid `ssh_config` file that describes the
tunnel the manager should create and monitor. For a complete overview see `man ssh_config`.
The target host given after `-t` should match a `Host` entry from your selected `ssh_config`
file.

All examples below assume `ssh-tunnel-manager` is in your `$PATH`.

#### Example Local Forward

Given a `local.conf` as below, and accepting default manager parameters, run:

> `ssh-tunnel-manager -F local.conf -t local_example`

`local.conf`
```
Host local_example
	Hostname local.example.com
	LocalForward 5432 localhost:5432
	User user
	IdentityFile ~/.ssh/id_ecdsa
	ServerAliveInterval 30s
	ServerAliveCountMax 2
	# Monitor Port
	LocalForward 63000 localhost:22
```

#### Example Remote Forward

Given a `remote.conf` as below, and accepting default manager parameters, run:

> `ssh-tunnel-manager -F remote.conf -t remote_example`

`remote.conf`
```
Host remote_example
	Hostname remote.example.com
	RemoteForward *:9999 localhost:22
	User user
	IdentityFile ~/.ssh/id_ecdsa
	ServerAliveInterval 30s
	ServerAliveCountMax 2
```
	
#### Example Dynamic Forward

Given a `dynamic.conf` as below, and accepting default manager parameters, run:

> `ssh-tunnel-manager -F dynamic.conf -t dynamic_example`

`dynamic.conf`
```
Host dynamic_example
	Hostname dynamic.example.com
	RemoteForward localhost:9999
	User user
	IdentityFile ~/.ssh/id_ecdsa
	ServerAliveInterval 30s
	ServerAliveCountMax 2
```
	
###	Manager Configuration

The following variables may be accepted as defaults, or overriden by a configuration
file passed to the `-e` option on execution:

* `UP_HOST`: IP or FQDN of the host to probe during network verification. Default: `example.com`
* `UP_PORT`: The port to attempt a connection to on `UP_HOST`, integer. Default: `443`
* `UP_TIMEOUT`: The timeout duration for the connection attempt to `UP_HOST:UP_PORT`
  * This value takes an integer followed by an option spec from `(ms, s, m, h)` if nmap-ncat is used. 
  Only seconds are supported for legacy netcat.
  * The default value is `1500ms` for nmap-ncat, or `2` for legacy netcat
* `INITIAL_MIN`: The initial random delay minimum value, in seconds. Default: `5`
  * Minimum delay before verifying network connection exists after a failure 
* `INITIAL_MAX`: The initial random delay maximum value, in seconds. Default: `15`
  * Maximum delay before verifying network connectoin exists after a failure
* `BACKOFF_MIN`: The secondary random delay minimum value, in seconds. Default: `30`
  * Minimum delay before the next monitor loop after a failure of the previous check
* `BACKOFF_MAX`: The secondary random delay maximum value, in seconds. Default: `300`
  * Maximum delay before the next monitor loop after a failure of the previous check
* `LOOP_SLEEP`: The duration for the monitor loop sleep period when there are no issues, Default: `60`

#### Example Manager Config

The following would override `UP_TIMEOUT`, and `BACKOFF_MAX`. All other default values
would remain in tact.

`mgr.env`
```
UP_TIMEOUT=600ms
BACKOFF_MAX=120
```

Used with `local.conf` from above:

> `ssh-tunnel-manager -F local.conf -t local_example -e mgr.env`

### Connection Monitoring

SSH Tunnel Manager takes a few different actions to validate a connection exists and is
working. These include verification that the SSH PID is running, confirmation that a 
default route exists and is capable of carrying traffic, and finally an optional check
that indeed the SSH connection itself carries traffic to the remote host. For the last
check the presence of a `LocalForward` declaration on the line immediately following a
 `# Monitor Port` comment in your tunnel configuration file is used. This is parsed to
 determine the port to use to attempt recieving an SSH banner. If the banner is present
 within the configured timeout, the connection is deemed OK, otherwise it's assumed
 down. In this case the zombie PID is killed so a new process can be started and managed.

### Recommendations

Rather than running this script ad-hoc and backgrounding it directly, create a service
using whichever system is preferred. It is likely worthwhile to capture stdout from
`ssh-tunnel-manager` and keep it in a persistent log, or to provide a replacement for
the `log()` function as suits the user.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.

Copyright (C) 2020-2021 Anthony Martinez
